/*
    ArchIO - a library for accessing archive formats in Qt applications
    Copyright (C) 2014  Martin Hoeher

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#ifndef ZIPARCHIVE_H
#define ZIPARCHIVE_H

#include "archive.h"
#include "zlib.h"

namespace AIO {

class ZipArchivePrivate;
class ZipArchive : public Archive
{
    Q_OBJECT
    Q_ENUMS( Method )
    Q_ENUMS( Strategy )
public:

    /**
       @brief Describes possible compression methods
     */
    enum Method {
        DeflateMethod = Z_DEFLATED //!< Use deflate algorithm.
    };

    /**
       @brief Compression strategy

       This enum describes possible values for tuning the compression algorithm.
     */
    enum Strategy {
        FilteredStrategy = Z_FILTERED, //!< Can be used for data produced by a filter (or predicator)
        HuffmanOnlyStrategy = Z_HUFFMAN_ONLY, //!< Force use of Huffman algorithm only
        RLEStrategy = Z_RLE, //!< Limit run length encoding (limit match distance to one). Useful e.g. in encoding image (PNG) data.
        FixedStrategy = Z_FIXED, //!< Do not use dynamic Huffman codes (which allows using simplified decoders in special applications).
        DefaultStrategy = Z_DEFAULT_STRATEGY //!< Useful for normal data
    };

    explicit ZipArchive(QIODevice* device, QObject *parent = 0);
    virtual ~ZipArchive();

    // Archive interface
    virtual FileInfoList listFiles();
    virtual QIODevice *readFile(const FileInfo &file);
    virtual QIODevice *writeFile(const FileInfo &file);

signals:

public slots:

protected:
    virtual void changeDevice(QIODevice *oldDevice, QIODevice *newDevice);

private:

    const QScopedPointer< ZipArchivePrivate > d_ptr;
    Q_DECLARE_PRIVATE( ZipArchive )

};

}

#endif // ZIPARCHIVE_H
