/*
    ArchIO - a library for accessing archive formats in Qt applications
    Copyright (C) 2013, 2014  Martin Hoeher

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include "archive.h"
#include "formats.h"

#include <QFile>
#include <QPointer>

/**
  @brief The ArchIO namespace
 */
namespace AIO {

/**
  @cond INTERNAL
  */

/**
   @brief The ArchivePrivate class
 */
class ArchivePrivate
{

    ArchivePrivate( Archive *parent ) :
        device( 0 ),
        lastErrorString( QString() ),
        q_ptr( parent )
    {
    }

    QPointer< QIODevice > device;
    QString lastErrorString;

    Archive *q_ptr;
    Q_DECLARE_PUBLIC( Archive )

    void setDevice( QIODevice *device ) {
        this->device = device;
    }

};

/**
  @endcond
  */

/**
   @class Archive
   @brief Read and write archive files

   The Archive class is used to read and write archive files of different
   formats.
 */

/**
   @brief Destructor
 */
Archive::~Archive()
{
}

/**
   @brief The device this archive operates on
 */
QIODevice *Archive::device() const
{
    const Q_D( Archive );
    return d->device;
}

/**
   @brief Sets the device

   Updates the @p device on which this archive operates.

   @sa changeDevice
 */
void Archive::setDevice(QIODevice *device)
{
    Q_D( Archive );
    QIODevice *oldDevice = d->device;
    d->device = device;
    changeDevice( oldDevice, device );
}

/**
   @brief The last error that has occurred

   Returns a string describing the last error that has occurred.

   @sa setLastErrorString
   @sa clearLastErrorString
   @sa hasError
 */
QString Archive::lastErrorString() const
{
    const Q_D( Archive );
    return d->lastErrorString;
}

/**
   @brief Clears the last error string
 */
void Archive::clearLastErrorString()
{
    Q_D( Archive );
    d->lastErrorString.clear();
}

/**
   @brief Returns whether an error occurred

   This function can be used to test whether an error occurred. Internally, this
   method tests whether the last error string is empty or not.

   @sa lastErrorString
   @sa clearLastErrorString
 */
bool Archive::hasError() const
{
    const Q_D( Archive );
    return !d->lastErrorString.isEmpty();
}

/**
   @brief Returns a list with information about all files in the archive

   This returns a listing of the files contained in the archive.

   @note This method should be overridden in classes implementing a concrete
   archive format.
 */
Archive::FileInfoList Archive::listFiles()
{
    return FileInfoList();
}

/**
   @brief Returns a QIODevice for reading the contents of a file in the archive

   Use this method to get a QIODevice that can be used to read the contents of
   a file within the archive. The file is specified via the @p file
   structure (usually, the fileName property is used to identify the file but
   concrete formats might use other properties as well).
   Returns either a valid QIODevice* that can be used for reading or 0
   in case there was a problem (like file not found or file cannot be
   openened for reading).

   @note Only one file within the archive can be accessed at the same time, i.e.
   there can only be one QIODevice at the same time that is used for reading
   or writing a file in the archive. Creating a QIODevice either via readFile()
   or writeFile() will invalidate any device previously created and any access to
   the previously created device might result in an access violation.

   @sa listFiles
   @sa writeFile
 */
QIODevice *Archive::readFile(const FileInfo &file)
{
    Q_UNUSED( file );
    return 0;
}

/**
   @brief Returns a QIODevice for writing the contents of a file in the archive

   Use this method to get a QIODevice that can be used for writing the
   contents of @p file within the archive. The interpretation of the file
   structure is up to the concrete archive format that has is used. In the
   simplest case, only the fileName property the structure is used but other
   members might be considered as well (e.g. for file permissions etc). Also
   consider that - again: depending on the concrete format - giving more
   information in the FileInfo structure (e.g. the uncompressed size of the file)
   might be helpful or even necessary. Please refer to the documentation of
   concrete plugins for mor information regarding this topic.

   @note Only one file within the archive can be accessed at the same time, i.e.
   there can only be one QIODevice at the same time that is used for reading
   or writing a file in the archive. Creating a QIODevice either via readFile()
   or writeFile() will invalidate any device previously created and any access to
   the previously created device might result in an access violation.

   @sa listFiles
   @sa readFile
 */
QIODevice *Archive::writeFile(const FileInfo &file)
{
    Q_UNUSED( file );
    return 0;
}

/**
   @brief Creates a new Archive

   Creates a new archive with the given @p format (e.g. "zip") and operating on
   the @p device and using the specified @p parent. If the format is not
   supported, 0 is returned.
 */
Archive *Archive::createArchive(QIODevice *device, const QString &format, QObject *parent)
{
    ArchiveFactory *factory = aioFormats.factoryForFormat( format );
    if ( factory )
    {
        return factory->createArchive( device, parent );
    }
    return 0;
}

/**
   @brief Creates a new Archive

   Creates a new archive for the given @p fileName and using @p parent. The
   format to be used is guessed from the file. If an appropriate format handler
   is found, an AIO::Archive is created and returned. Otherwise, 0 is returned.
 */
Archive *Archive::createArchive(const QString fileName, QObject *parent)
{
    ArchiveFactory *factory = aioFormats.factoryForFile( fileName );
    if ( factory )
    {
        QFile *file = new QFile( fileName );
        if ( file->open( QIODevice::ReadWrite ) )
        {
            Archive *result = factory->createArchive( file, parent );
            if ( result )
            {
                file->setParent( result );
                return result;
            }
        }
    }
    return 0;
}

/**
   @brief Constructor

   Creates a new archive which operates on the given @p device. The
   open mode of the device determines the mode in which the archive
   operates, i.e. if you want to create a new archive, you have to
   create a new QIODevice first which can write.

   @note Plugin developers should use this constructor as their base.
         The other constructors provided by the Archive class are convenience
         constructors.
 */
Archive::Archive(QIODevice *device, QObject *parent ) :
    QObject( parent ),
    d_ptr( new ArchivePrivate( this ) )
{
    Q_D( Archive );
    d->device = 0;
    setDevice( device );
}

/**
   @brief Apply to the new device

   This method is called when the setDevice() method is used (also initially
   in the constructor) to update the device the archive is working on. Override
   this method in a concrete class implementing a concrete archive format to
   adapt to the @p newDevice. The @p oldDevice is also available (if required).

   @note Both oldDevice and newDevice might be null.
 */
void Archive::changeDevice(QIODevice *oldDevice, QIODevice *newDevice)
{
    // empty!
    Q_UNUSED( oldDevice );
    Q_UNUSED( newDevice );
}

/**
   @brief Sets the last error string

   This updates the lastError property to @p errorString. Subclasses should
   use this method to update the error string to something meaningful which
   helps troubleshoot any problems.
 */
void Archive::setLastErrorString(const QString &errorString)
{
    Q_D( Archive );
    d->lastErrorString = errorString;
}

/**
   @class AIO::Archive::FileInfo
   @brief Provide information about a file inside an archive.

   The FileInfo class is used to carry information about a single file
   (or directory) within an archive. Internally, it uses a QVariantMap for storage,
   allowing format implementations to put any value into the info structure.

   The class provides special accessors for certain (global) file attributes
   that are assumed to be part in any format.

   @note The keys used by the accessors of the FileInfo class are names
    after the corresponding properties of the class (e.g. the key used for
    the "fileName" property is "fileName" and so on. If a plugin wants to
    put extra information into the FileInfo structure, it ideally would
    prefix it with it's format name, e.g. for an attribute that is specific
    to the zip format, one would choose "zip.mySpecialAttr".
 */

/**
   @brief Creates a new FileInfo object

   @param fileName The (full) name of the file inside the archive.
   @param comment An (optional) comment attached to the file.
   @param size The (uncompressed) size of the file.
   @param compressedSize The size of the file within the archive.
 */
Archive::FileInfo::FileInfo(const QByteArray &fileName,
                            const QByteArray &comment,
                            qint64 size,
                            qint64 compressedSize) :
  m_values()
{
    setFileName( fileName );
    setComment( comment );
    setSize( size );
    setCompressedSize( compressedSize );
    setValid( true );
}

/**
   @brief Creates a new invalid FileInfo object.
 */
Archive::FileInfo::FileInfo()
{
    setValid( false );
}

/**
   @brief Destructor
 */
Archive::FileInfo::~FileInfo()
{
}

/**
   @brief The name and path of the file within the archive.
 */
QByteArray Archive::FileInfo::fileName() const
{
    return m_values.value( "fileName", QByteArray() ).toByteArray();

}

/**
   @brief Sets the fileName property.
 */
void Archive::FileInfo::setFileName(const QByteArray &fileName)
{
    m_values.insert( "fileName", fileName );
}

/**
   @brief An optional comment attached to the file.
 */
QByteArray Archive::FileInfo::comment() const
{
    return m_values.value( "comment", QByteArray() ).toByteArray();
}

/**
   @brief Sets the comment property.
 */
void Archive::FileInfo::setComment(const QByteArray &comment)
{
    m_values.insert( "comment", comment );
}

/**
   @brief The uncompressed size of the file.
 */
qint64 Archive::FileInfo::size() const
{
    return m_values.value( "size", -1 ).toLongLong();
}

/**
   @brief Sets the size property.
 */
void Archive::FileInfo::setSize(qint64 size)
{
    m_values.insert( "size", size );
}

/**
   @brief The compressed size of the file.
 */
qint64 Archive::FileInfo::compressedSize() const
{
    return m_values.value( "compressedSize", -1 ).toLongLong();
}

/**
   @brief Sets the compressedSize property.
 */
void Archive::FileInfo::setCompressedSize(qint64 compressedSize)
{
    m_values.insert( "compressedSize", compressedSize );
}

/**
   @brief The compression level

   Returns (if known/set) the level of compression used for the file. If
   no level is set, returns @p defaultValue.
 */
int Archive::FileInfo::level(int defaultValue) const
{
    return m_values.value( "level", defaultValue ).toInt();
}

/**
   @brief Sets the level property.
 */
void Archive::FileInfo::setLevel(int level)
{
    m_values.insert( "level", level );
}

/**
   @brief The password required for the file
 */
QByteArray Archive::FileInfo::password() const
{
    return m_values.value( "password", QByteArray() ).toByteArray();
}

/**
   @brief Sets the password property
 */
void Archive::FileInfo::setPassword(const QByteArray &password)
{
    m_values.insert( "password", password );
}

/**
   @brief Returns whether the FileInfo is valid or not.
 */
bool Archive::FileInfo::isValid() const
{
    return m_values.value( "isValid", false ).toBool();
}

/**
   @brief Sets the isValid property.
 */
void Archive::FileInfo::setValid(bool valid)
{
    m_values.insert( "isValid", valid );
}

/**
   @brief Returns an error string.

   This property can be used to indicate that e.g. fetching information about a
   file in an archive encountered some problems.
 */
QString Archive::FileInfo::errorString() const
{
    return m_values.value( "errorString", QString() ).toString();
}

/**
   @brief Sets the errorString property.
 */
void Archive::FileInfo::setErrorString(const QString &errorString)
{
    m_values.insert( "errorString", errorString );
}

/**
   @brief Creates an invalid FileInfo object with an @p errorString
 */
Archive::FileInfo Archive::FileInfo::createError(const QString &errorString)
{
    FileInfo result;
    result.setErrorString( errorString );
    return result;
}

/**
 * @brief The variant map used to store the properties of the file
 */
QVariantMap &Archive::FileInfo::values()
{
  return m_values;
}

/**
 * @brief The variant map used to store the properties of the file
 */
const QVariantMap &Archive::FileInfo::values() const
{
  return m_values;
}

} // namespace AIO
