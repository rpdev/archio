/*
    ArchIO - a library for accessing archive formats in Qt applications
    Copyright (C) 2013, 2014  Martin Hoeher

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#ifndef FORMATS_H
#define FORMATS_H

#include "archivefactory.h"
#include "archio_global.h"

#include <QObject>
#include <QScopedPointer>
#include <QVector>

/**
  @brief Helper macro: Access the global AIO::Formats instance

  This macro is a shorthand to the AIO::Formats::instance() method which
  returns the global instance of the Formats class.
  */
#define aioFormats (AIO::Formats::instance())

namespace AIO {

class FormatsPrivate;
class ARCHIOSHARED_EXPORT Formats : public QObject
{
    Q_OBJECT
public:

    typedef QVector<ArchiveFactory*> FormatsList;

    virtual ~Formats();

    static Formats &instance();

    FormatsList formats() const;

    ArchiveFactory *factoryForFile( const QString &fileName ) const;
    ArchiveFactory *factoryForFormat(const QString &format ) const;

signals:

public slots:

    bool registerFormat( ArchiveFactory *factory );

private:

    explicit Formats(QObject *parent = 0);

    const QScopedPointer< FormatsPrivate > d_ptr;
    Q_DECLARE_PRIVATE( Formats )

};

}

#endif // FORMATS_H
