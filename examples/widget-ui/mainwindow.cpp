#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "../../lib/ziparchive.h"

#include <QFile>
#include <QFileDialog>
#include <QInputDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect( ui->actionQuit, SIGNAL(triggered()), qApp, SLOT(quit()) );
    connect( ui->actionOpen, SIGNAL(triggered()), this, SLOT(selectFile()) );
    connect( ui->actionList_Files, SIGNAL(triggered()),
             this, SLOT(listArchiveContents()) );
    connect( ui->actionClear_Log, SIGNAL(triggered()), this, SLOT(clearLog()) );
    connect( ui->actionShow_File, SIGNAL(triggered()),
             this, SLOT(showFile()) );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::printInfo(const QString &line)
{
    ui->log->append( line );
}

void MainWindow::printError(const QString &line)
{
    ui->log->append( "<span style=\"color: red\">" + line + "</span>" );
}

void MainWindow::selectFile()
{
    QString fileName = QFileDialog::getOpenFileName(
                this,
                tr( "Select Archive File" ),
                QString(), // dir
                QString()  // filter
                );
    if ( !fileName.isEmpty() )
    {
        ui->fileNameEdit->setText( fileName );
    }
}

void MainWindow::listArchiveContents()
{
    QFile file( ui->fileNameEdit->text() );
    if ( file.open( QIODevice::ReadOnly ) )
    {
        AIO::ZipArchive archive( &file );
        AIO::ZipArchive::FileInfoList infos = archive.listFiles();
        if ( archive.hasError() )
        {
            printError( "Unable to list files: " + archive.lastErrorString() );
        } else
        {
            foreach ( const AIO::Archive::FileInfo info, infos )
            {
                printInfo( info.fileName );
            }
        }
    } else
    {
        printError( "Unable to open file: " + file.errorString() );
    }
}

void MainWindow::showFile()
{
    QFile file( ui->fileNameEdit->text() );
    if ( file.open( QIODevice::ReadOnly ) )
    {
        AIO::ZipArchive archive( &file );
        AIO::ZipArchive::FileInfoList files = archive.listFiles();
        QStringList stringList;
        foreach ( AIO::Archive::FileInfo fi, files )
        {
            stringList << fi.fileName;
        }
        if ( stringList.isEmpty() )
        {
            printError( "No files in archive..." );
        } else
        {
            bool ok;
            QString file = QInputDialog::getItem(
                        this,
                        tr( "Select file..." ),
                        tr( "Please select a file for being displayed..." ),
                        stringList,
                        -1,
                        false,
                        &ok );
            if ( ok )
            {
                QIODevice *device = archive.readFile( file );
                if ( device )
                {
                    printInfo( "Contents of " + file );
                    QByteArray data = device->readAll();
                    printInfo( data );
                } else
                {
                    printError( "Unable to read file: " + archive.lastErrorString() );
                }
            }
        }
    } else
    {
        printError( "Unable to open file: " + file.errorString() );
    }
}

void MainWindow::clearLog()
{
    ui->log->clear();
}
