/*
    ArchIO - a library for accessing archive formats in Qt applications
    Copyright (C) 2014  Martin Hoeher

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#include <QString>
#include <QtTest>

#include "formats.h"
#include "archive.h"
#include "archivefactory.h"

class FormatsTest : public QObject
{
    Q_OBJECT

public:
    FormatsTest();

private:

    QCoreApplication *app;

private Q_SLOTS:

    void initTestCase();

    // test for presence and proper handling of zip format
    void zipPluginPresentTest();
    void canHandleZipFileTest();
    void canHandleZipFormatTest();
    void zipQuickCreationWrapperTest();

    // test zip compression/uncompression feature
    void zipCompressUncompressBufferTest();
};

FormatsTest::FormatsTest()
{
}

void FormatsTest::initTestCase()
{
    QDir appDir = QCoreApplication::applicationDirPath();
    appDir.cdUp();
    appDir.cdUp();
#ifdef Q_OS_WIN
    // need to go up one additional level because of the debug/release subdirectories
    appDir.cdUp();
#endif
    appDir.cd( "ArchIO" );
    appDir.cd( "plugins" );
    QCoreApplication::addLibraryPath( appDir.path() );
}

void FormatsTest::zipPluginPresentTest()
{
    bool foundZipFormat = false;
    AIO::Formats::FormatsList formats = aioFormats.formats();
    foreach ( AIO::ArchiveFactory *factory, formats ) {
        if ( factory->formatName() == "Zip" ) {
            foundZipFormat = true;
            break;
        }
    }
    QVERIFY2(foundZipFormat, "The Zip format plugin does not seem to be loaded");
}

void FormatsTest::canHandleZipFileTest()
{
    QVERIFY2( aioFormats.factoryForFile( "/home/test/sample.zip" ) != 0,
             "Failed to find handler for *.zip files." );
}

void FormatsTest::canHandleZipFormatTest()
{
    QVERIFY2( aioFormats.factoryForFormat( "zip" ) != 0,
              "Failed to find handler for zip format." );
}

void FormatsTest::zipQuickCreationWrapperTest()
{
    QBuffer buffer;
    buffer.open( QIODevice::ReadWrite );
    AIO::Archive *archive = AIO::Archive::createArchive( &buffer, "zip" );
    QVERIFY2( archive != 0, "Unable to use convenience constructor method for zip format" );
    if ( archive ) {
        delete archive;
    }
}

void FormatsTest::zipCompressUncompressBufferTest()
{
    AIO::ArchiveFactory *factory = aioFormats.factoryForFormat( "zip" );
    QVERIFY2( factory != 0, "Unable to find format factory for zip format" );

    QBuffer buffer;
    buffer.open( QIODevice::ReadWrite );
    AIO::Archive *archive = factory->createArchive( &buffer, this );
    QVERIFY2( archive != 0, "Unable to create Zip archive" );

    AIO::Archive::FileInfo fileInfo( "test.bin" );
    QIODevice *device = archive->writeFile( fileInfo );
    QVERIFY2( device != 0, "Cannot add new file to Zip archive." );

    for ( int i = 0; i < 10240; ++i ) {
        QCOMPARE( device->pos(), static_cast< qint64 >( i * sizeof( i ) ) );
        int val = i % 10;
        QCOMPARE( device->write( reinterpret_cast< const char* >( &val ), sizeof( val ) ), static_cast< qint64 >( sizeof( val ) ) );
        QCOMPARE( device->size(), static_cast< qint64 >( ( i + 1 ) * sizeof( i ) ) );
        QCOMPARE( device->pos(), static_cast< qint64 >( ( i + 1 ) * sizeof( i ) ) );
    }

    device = archive->readFile( fileInfo );
    QVERIFY2( device != 0, "Cannot read file from Zip archive" );

    int val;
    for ( int i = 0; i < 10240; ++i ) {
        QCOMPARE( device->pos(), static_cast< qint64 >( i * sizeof( i ) ) );
        QCOMPARE( device->read( reinterpret_cast< char* >( &val ), sizeof( val ) ), static_cast< qint64 >( sizeof( val ) ) );
        QCOMPARE( val, i % 10 );
        QCOMPARE( device->pos(), static_cast< qint64 >( ( i + 1 ) * sizeof( i ) ) );
    }

    delete archive;
}

QTEST_MAIN(FormatsTest)

#include "tst_formatstest.moc"
