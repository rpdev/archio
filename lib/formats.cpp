/*
    ArchIO - a library for accessing archive formats in Qt applications
    Copyright (C) 2013, 2014  Martin Hoeher

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include "formats.h"
#include "archiveformatplugin.h"

#include <QCoreApplication>
#include <QDir>
#include <QFileInfo>
#include <QMutex>
#include <QPluginLoader>
#include <QRegExp>
#include <QVector>

namespace AIO {

/**
  @cond INTERNAL
  */
/**
   @brief The FormatsPrivate class
   @internal
 */
class FormatsPrivate
{
public:

    FormatsPrivate( Formats *parent ) :
        q_ptr( parent ),
        lock( QMutex::NonRecursive ),
        initialized( false ) {
    }

    Formats::FormatsList factories;

    void initOnce() {
        QMutexLocker l( &lock );
        if ( !initialized ) {
            loadFormatPlugins();
            initialized = true;
        }
    }

private:

    Formats *q_ptr;
    Q_DECLARE_PUBLIC( Formats )

    QMutex lock;
    bool initialized;

    void loadFormatPlugins() {
        // Load static plugins
        foreach ( QObject* plugin, QPluginLoader::staticInstances() ) {
            addPlugin( plugin );
        }

        // Load dynamic plugins
        foreach ( QString searchPath, QCoreApplication::libraryPaths() ) {
            QDir dir( searchPath );
            if ( dir.cd( "archio" ) ) {
                foreach ( QString entry, dir.entryList( QDir::Files ) ) {
                    QString fullPath = dir.absoluteFilePath( entry );
                    QPluginLoader loader( fullPath );
                    QObject *plugin = loader.instance();
                    if ( plugin ) {
                        addPlugin( plugin );
                    }
                }
            }
        }
    }

    void addPlugin( QObject* plugin ) {
        Q_Q( Formats );
        FormatPlugin *formatPlugin = qobject_cast< FormatPlugin* >( plugin );
        if ( formatPlugin ) {
            q->registerFormat( formatPlugin->createFactory() );
        }
    }

};

/**
  @endcond
  */

/**
   @class Formats
   @brief Manages supported archive formats

   The Formats class is used to manage the available archive formats
   that can be handled. ArchIO uses a plugin system. This system will
   automatically search for plugins in all plugin search paths. If you want
   to add new plugins, you can use QCoreApplication::addLibraryPath(). ArchIO
   assumes its plugins to be in a subdirectory called "archio". It will probe
   all files found this way and - on success - add them to the list of known
   formats.

   If you want to provide an own format but without having to write a complete
   Qt plugin, you can use the registerFormat() method, which takes a
   ArchiveFactory instance.
 */

/**
   @brief Destructor
 */
Formats::~Formats()
{
}

/**
   @brief Returns the global instance of the formats class.

   This returns the global instance of the Formats class.
 */
Formats &Formats::instance()
{
    static Formats inst;
    inst.d_ptr->initOnce();
    return inst;
}

/**
   @brief The list of registered formats

   This returns a list of ArchiveFactory objects registered with the
   Formats class.

 */
Formats::FormatsList Formats::formats() const
{
    const Q_D( Formats );
    return d->factories;
}

/**
   @brief Returns a format factory that can be used to treat the given file

   This will search in the list of registered formats for a factory where
   at least one format filter matches the given file name. If such a factory is
   found, it is returned. If no factory fulfils the criteria, NULL is returned.

   @note This method does not probe the file itself. Instead, it merely operates
         on the file's name.

   @sa AIO::ArchiveFactory::formatFilter()
 */
ArchiveFactory *Formats::factoryForFile(const QString &fileName) const
{
    const Q_D( Formats );
    QFileInfo fi( fileName );
    foreach ( ArchiveFactory *factory, d->factories ) {
        foreach ( QString filter, factory->formatFilter() ) {
            QRegExp re( filter, Qt::CaseInsensitive, QRegExp::Wildcard );
            if ( re.indexIn( fi.fileName() ) == 0 ) {
                return factory;
            }
        }
    }
    return 0;
}

/**
   @brief Returns a factory for handling the given format.

   This returns a factory that can be used to handle the given @p format. The
   format is the file name extension of files that can be handled by a
   ArchiveFactory. Internally, this uses the factoryForFile() method in order
   to look up an appropriate format factory.

   @sa AIO::Formats::factoryForFile()

 */
ArchiveFactory *Formats::factoryForFormat(const QString &format) const
{
    return factoryForFile( "sample." + format );
}

/**
   @brief Registers a new format

   Registers a new format by adding the @p factory to the Formats object.
   The ownership of the factory is taken over by this object.

   @returns True if the format has been installed or false if
   either the factory object is null or already has been registered
   before.
 */
bool Formats::registerFormat(ArchiveFactory *factory)
{
    Q_D( Formats );
    if ( factory && !d->factories.contains( factory ) ) {
        d->factories << factory;
        factory->setParent( this );
        return true;
    }
    return false;
}

/**
   @brief Constructor
 */
Formats::Formats(QObject *parent) :
    QObject(parent),
    d_ptr( new FormatsPrivate( this ) )
{
}

}
