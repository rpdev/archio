#-------------------------------------------------
#
# Project created by QtCreator 2014-01-08T18:57:30
#
#-------------------------------------------------

QT       -= gui

TARGET = zip
TEMPLATE = lib

DEFINES += ARCHIO_ZIP_PLUGIN
win32:DEFINES += QT_VISIBILITY_AVAILABLE

SOURCES += \
    ziparchive.cpp \
    ziparchivefactory.cpp \
    ../../3rdParty/unzip11/unzip.c \
    ../../3rdParty/unzip11/zip.c \
    ../../3rdParty/unzip11/ioapi.c \
    zipformatplugin.cpp

HEADERS += \
    ziparchive.h \
    ziparchivefactory.h \
    zipformatplugin.h

CONFIG += plugin

INCLUDEPATH += ../../lib/ ../../3rdParty/unzip11

msvc {
  message( "Compiling against ArchIO provided zlib headers for MSVC" )
  INCLUDEPATH += ../../3rdParty/QtZlibMSVC
} else:exists( $$[QT_INSTALL_HEADERS]/QtZlib/zlib.h ) {
  message( "Compiling against Qt zlib headers" )
  INCLUDEPATH += $$[QT_INSTALL_HEADERS]/QtZlib
} else:win32 {
  message( "Compiling against ArchIO provided zlib headers" )
  INCLUDEPATH += ../../3rdParty/QtZlib
} else {
  message( "Compiling against system zlib headers" )
}

msvc {
  DEFINES += __attribute__(...)=
}

win32 {
  greaterThan( QT_MAJOR_VERSION, 4 ) {
    message( "Compiling using Z_PREFIX (Qt5 mode)" )
    DEFINES += Z_PREFIX
  }
}

LIBS += -L../../ArchIO/lib -lArchIO
unix:LIBS += -lz

# File handling on MacOS is by default 64bit... add defines for unzip:
macx {
  DEFINES += fopen64=fopen fseeko64=fseek ftello64=ftell
}

DESTDIR = ../../ArchIO/plugins/archio

# For installation to existing Qt installation:
# 1. Copy plugin into Qt's plugin directory
target.path = $$[QT_INSTALL_PLUGINS]/archio
INSTALLS += target
