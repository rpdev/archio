#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    void printInfo( const QString &line );
    void printError( const QString &line );

private slots:

    void selectFile();
    void listArchiveContents();
    void showFile();
    void clearLog();
};

#endif // MAINWINDOW_H
