TEMPLATE = subdirs
SUBDIRS += lib \
#    examples \
    plugins \
    tests
CONFIG += ordered
OTHER_FILES += README COPYING COPYING.LESSER templates/license-header.txt
