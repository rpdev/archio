#-------------------------------------------------
#
# Project created by QtCreator 2013-12-12T21:57:42
#
#-------------------------------------------------

QT       -= gui

TARGET = ArchIO
TEMPLATE = lib

DEFINES += ARCHIO_LIBRARY

!win32:VERSION = 0.0.0

SOURCES += archive.cpp \
           formats.cpp \
           archivefactory.cpp

HEADERS += archive.h\
        archio_global.h \
        formats.h \
        archivefactory.h \
        archiveformatplugin.h \
        ArchIO

OTHER_FILES += \
    Doxyfile

DESTDIR = ../ArchIO/lib

# For installation to existing Qt installation:
# 1. Copy library into Qt's library directory
target.path = $$[QT_INSTALL_LIBS]
INSTALLS += target
# 2. Copy headers into Qt's include directory
headers.files = $$HEADERS
headers.path = $$[QT_INSTALL_HEADERS]/ArchIO
INSTALLS += headers
