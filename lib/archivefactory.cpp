/*
    ArchIO - a library for accessing archive formats in Qt applications
    Copyright (C) 2013, 2014  Martin Hoeher

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include "archivefactory.h"

#include <QStringList>

namespace AIO {

/**
  @cond INTERNAL
  */
/**
   @brief The ArchiveFactoryPrivate class
 */
class ArchiveFactoryPrivate
{

    ArchiveFactoryPrivate( ArchiveFactory *parent ) :
        q_ptr( parent )
    {

    }

    QString formatName;
    QStringList formatFilter;

    ArchiveFactory *q_ptr;
    Q_DECLARE_PUBLIC( ArchiveFactory )
};

/**
  @endcond
  */

/**
  @class ArchiveFactory
  @brief Creates AIO::Archive objects

  ArchIO uses the Factory pattern in order to dynamically
  create Archive objects. A format plugin has to provide a specialized
  ArchiveFactory class in order to properly integrate into the framework.
  Providing such a class allows the format to be dynamically integrated and thus
  allows extending any application using the framework without any modifications.
  */
/**
   @brief Constructor

   @param formatName The name of the format (can be used in listings, ...)
   @param formatFilter A list of globs (e.g. *.zip) that can be used to identify
          a format by its file name.
   @param parent The parent object for the factory.
 */
ArchiveFactory::ArchiveFactory(const QString &formatName, const QStringList &formatFilter, QObject *parent) :
    QObject(parent),
    d_ptr( new ArchiveFactoryPrivate( this ) )
{
    d_ptr->formatName = formatName;
    d_ptr->formatFilter = formatFilter;
}

ArchiveFactory::~ArchiveFactory()
{
}

/**
   @brief The (human readable) name of the format.
 */
QString ArchiveFactory::formatName() const
{
    const Q_D( ArchiveFactory );
    return d->formatName;
}

/**
   @brief The filter that can be used to identify files of this type

   Returns a list of globs (e.g. *.zip) that can be used to identify files of
   this type.
 */
QStringList ArchiveFactory::formatFilter() const
{
    const Q_D( ArchiveFactory );
    return d->formatFilter;
}

/**
   @brief Creates a new archive for working on the given @p device and with the given @p parent
   @return An Archive that operates on the given device or 0 in case no Archive
   could be created.

   @note This method should be overridden in classes implementing a concrete
   factory.
 */
Archive *ArchiveFactory::createArchive(QIODevice *device, QObject *parent)
{
    Q_UNUSED( device );
    Q_UNUSED( parent );
    return 0;
}

/**
   @brief Checks whether this factory can create archives for the given @p device
   @return True in case this factory can create an Archive for the given device
   or false otherwise.

   qnote This method should be overridden in classes implementing a concrete
   factory.
 */
bool ArchiveFactory::supportsDevice(QIODevice *device)
{
    Q_UNUSED( device );
    return false;
}

} // namespace AIO
