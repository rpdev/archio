/*
    ArchIO - a library for accessing archive formats in Qt applications
    Copyright (C) 2014  Martin Hoeher

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#ifndef ARCHIVEFORMATPLUGIN_H
#define ARCHIVEFORMATPLUGIN_H

#include "archivefactory.h"

#include <QtPlugin>

namespace AIO {

/**
   @brief Interface for format plugins

   This interface describes plugins that extend the list of supported formats
   of ArchIO.
 */
class FormatPlugin
{
public:

    /**
       @brief Destructor

       Required to stop some compilers from complaining.
     */
    virtual ~FormatPlugin() {}

    /**
       @brief Creates a new ArchiveFactory.

       This method creates and returns a new instance of an ArchiveFactory.
       It is used when the AIO::Formats class loads all plugins.

       @note The AIO::Formats class will take over ownership of the returned
       object.
     */
    virtual ArchiveFactory *createFactory() = 0;
};

}

Q_DECLARE_INTERFACE( AIO::FormatPlugin, "net.rpdev.archio.FormatPlugin/1.0" )

#endif // ARCHIVEFORMATPLUGIN_H
