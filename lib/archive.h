/*
    ArchIO - a library for accessing archive formats in Qt applications
    Copyright (C) 2013, 2014  Martin Hoeher

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#ifndef AIO_ARCHIVE_H
#define AIO_ARCHIVE_H

#include "archio_global.h"

#include <QIODevice>
#include <QObject>
#include <QVariantMap>
#include <QVector>

namespace AIO {

class ArchivePrivate;

class ARCHIOSHARED_EXPORT Archive : public QObject
{

    Q_OBJECT
    Q_PROPERTY( QIODevice* device READ device WRITE setDevice )
    Q_PROPERTY( QString lastErrorString READ lastErrorString WRITE setLastErrorString )

public:

    class ARCHIOSHARED_EXPORT FileInfo
    {
    public:
        explicit FileInfo( const QByteArray &fileName,
                  const QByteArray &comment = QByteArray(),
                  qint64 size = -1,
                  qint64 compressedSize = -1 );
        explicit FileInfo();
        virtual ~FileInfo();

        QByteArray fileName() const;
        void setFileName(const QByteArray &fileName );

        QByteArray comment() const;
        void setComment(const QByteArray &comment );

        qint64 size() const;
        void setSize( qint64 size );

        qint64 compressedSize() const;
        void setCompressedSize( qint64 compressedSize );

        int level( int defaultValue = -1 ) const;
        void setLevel( int level );

        QByteArray password() const;
        void setPassword( const QByteArray &password );

        bool isValid() const;
        void setValid( bool valid );

        QString errorString() const;
        void setErrorString( const QString &errorString );

        static FileInfo createError( const QString &errorString );

        QVariantMap& values();
        const QVariantMap &values() const;

    private:

        QVariantMap m_values;

    };

    typedef QVector< FileInfo > FileInfoList;

    virtual ~Archive();

    QIODevice *device() const;
    void setDevice( QIODevice *device );

    QString lastErrorString() const;
    void clearLastErrorString();
    bool hasError() const;

    virtual FileInfoList listFiles();
    virtual QIODevice *readFile( const FileInfo& file );
    virtual QIODevice *writeFile( const FileInfo& file );

    static Archive *createArchive( QIODevice *device,
                                   const QString &format,
                                   QObject *parent = 0 );
    static Archive *createArchive( const QString fileName, QObject* parent );

protected:

    explicit Archive( QIODevice *device, QObject* parent );

    virtual void changeDevice( QIODevice* oldDevice, QIODevice* newDevice );

    void setLastErrorString( const QString& errorString );

private:

    const QScopedPointer< ArchivePrivate > d_ptr;
    Q_DECLARE_PRIVATE( Archive )

};


} // namespace AIO
#endif // AIO_ARCHIVE_H
