#-------------------------------------------------
#
# Project created by QtCreator 2014-01-09T13:57:11
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_formatstest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_formatstest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

INCLUDEPATH += ../../lib
LIBS += -L../../ArchIO/lib -lArchIO
