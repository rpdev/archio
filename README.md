# ArchIO - a library for accessing archive file formats from Qt applications

ArchIO is a leightweight library for accessing archive file formats from within
Qt applications.

## Architecture

ArchIO consists basically of two parts: The actual library (against which a
Qt application can be written) defines the interfaces. These interfaces
are following Qt's internal structure, heavily building on the QIODevice
APIs (and hence making it easy to integrate ArchIO in different use cases).

The second part is the plugins layer - access to different archive file formats is
implemented as one plugin per format. This makes adding new formats easy and even
let's applications add their own formats (if desired).

## Supported Archive Formats

Currently, ArchIO supports the following formats:

### Zip (including Zip64 support)

The Zip file format is supported, including support for large files (aka Zip64).

## Licensing

ArchIO is released under the terms of the GNU Lesser General Public License
version 2.1 (or later, at your choice). You should have received a copy of
the license together with this file.

The Zip plugin uses zlib and MiniZip libraries for reading and writing Zip
files. See http://www.zlib.net/ for more details.
