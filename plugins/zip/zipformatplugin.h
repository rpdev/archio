/*
    ArchIO - a library for accessing archive formats in Qt applications
    Copyright (C) 2014  Martin Hoeher

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#ifndef AIO_ZIPFORMATPLUGIN_H
#define AIO_ZIPFORMATPLUGIN_H

#include "archiveformatplugin.h"

#include <QObject>

/**
  @cond INTERNAL
 */

namespace AIO {

class ZipFormatPlugin : public QObject, public FormatPlugin
{
    Q_OBJECT
    Q_INTERFACES(AIO::FormatPlugin)
#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
    Q_PLUGIN_METADATA(IID "net.rpdev.archio.FormatPlugin/1.0")
#endif
public:
    explicit ZipFormatPlugin(QObject *parent = 0);

signals:

public slots:


    // FormatPlugin interface
public:
    virtual ArchiveFactory *createFactory();

};

} // namespace AIO

/**
  @endcond
  */

#endif // AIO_ZIPFORMATPLUGIN_H
