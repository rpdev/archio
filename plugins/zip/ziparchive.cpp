/*
    ArchIO - a library for accessing archive formats in Qt applications
    Copyright (C) 2014  Martin Hoeher

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include "ziparchive.h"

#include "ioapi.h"
#include "unzip.h"
#include "zip.h"

#include <QDebug>

namespace AIO
{


/**
  @cond INTERNAL
 */
class ZipArchiveFileWriter : public QIODevice
{

public:

    explicit ZipArchiveFileWriter( zipFile file, Archive::FileInfo fileInfo, QObject* parent ) :
        QIODevice( parent ),
        file( file ),
        bytesWritten( 0 ),
        fileInfo( fileInfo )
    {
        open( QIODevice::WriteOnly | QIODevice::Unbuffered );
    }

    virtual ~ZipArchiveFileWriter()
    {
        zipCloseFileInZip( file );
        zipClose( file, 0 );
    }

    virtual bool isSequential() const
    {
        return true;
    }

    virtual qint64 pos() const
    {
        return bytesWritten;
    }

    virtual qint64 size() const
    {
        return bytesWritten;
    }

protected:

    virtual qint64 writeData(const char *data, qint64 len)
    {
        if ( zipWriteInFileInZip( file, data, len ) == ZIP_OK )
        {
            bytesWritten += len;
            return len;
        } else
        {
            setErrorString( tr( "Failed to write data to Zip archive" ) );
            return 0;
        }
    }

    virtual qint64 readData(char *data, qint64 maxlen)
    {
        Q_UNUSED( data );
        Q_UNUSED( maxlen );
        return 0;
    }

private:

    zipFile file;
    qint64 bytesWritten;
    Archive::FileInfo fileInfo;

};

/**
   @brief Implements a QIODevice reading from a file within a Zip
   @internal
 */
class ZipArchiveFileReader : public QIODevice
{

public:

    explicit ZipArchiveFileReader( unzFile file, Archive::FileInfo fileInfo, QObject* parent ) :
        QIODevice( parent ),
        file( file ),
        bytesRead( 0 ),
        fileInfo( fileInfo )
    {
        open( QIODevice::ReadOnly | QIODevice::Unbuffered );
    }

    virtual ~ZipArchiveFileReader()
    {
        unzCloseCurrentFile( file );
        unzClose( file );
    }

protected:

private:
    unzFile file;

    // QIODevice interface
public:

    virtual bool isSequential() const
    {
        return true;
    }

    virtual qint64 pos() const
    {
        return bytesRead;
    }
    virtual qint64 size() const
    {
        return fileInfo.size();
    }

protected:
    virtual qint64 readData(char *data, qint64 maxlen)
    {
        qint64 br = unzReadCurrentFile( file, static_cast< voidp >( data ), maxlen );

        if ( br >= 0 )
        {
            bytesRead += br;
            return br;
        } else
        {
            setErrorString( tr( "Encountered error during reading from file from zip archive: Error code %1" ).arg( br ) );
            return 0;
        }
    }

    virtual qint64 writeData(const char *data, qint64 len)
    {
        Q_UNUSED( data );
        Q_UNUSED( len );
        qWarning() << "ZipArchiveFileReader cannot be used to write data to zip file";
        return 0;
    }

private:

    qint64 bytesRead;
    Archive::FileInfo fileInfo;

};

class ZipArchivePrivate
{

    zlib_filefunc64_def fileFuncs;
    QIODevice *currentFileIO;

    ZipArchivePrivate( ZipArchive *parent ) :
        fileFuncs(),
        currentFileIO( 0 ),
        q_ptr( parent )
    {
        // set up the I/O handler for zlib
        fileFuncs.opaque = this;
        fileFuncs.zopen64_file = zlib_open;
        fileFuncs.zread_file = zlib_read;
        fileFuncs.zwrite_file = zlib_write;
        fileFuncs.ztell64_file = zlib_tell;
        fileFuncs.zseek64_file = zlib_seek;
        fileFuncs.zclose_file = zlib_close;
        fileFuncs.zerror_file = zlib_testerror;
    }

    QIODevice *device() const {
        const Q_Q( ZipArchive );
        return q->device();
    }

    ZipArchive *q_ptr;
    Q_DECLARE_PUBLIC( ZipArchive )

    static voidpf ZCALLBACK zlib_open( voidpf opaque,
                                       const void* filename,
                                       int mode )
    {
        Q_UNUSED( filename );
        Q_UNUSED( mode );
        return reinterpret_cast< ZipArchivePrivate* >( opaque )->device();
    }

    static uLong ZCALLBACK zlib_read( voidpf opaque,
                                      voidpf stream,
                                      void* buf,
                                      uLong size )

    {
        Q_UNUSED( opaque );
        QIODevice *device = reinterpret_cast< QIODevice* >( stream );
        return device->read( static_cast< char* >( buf ), size );
    }

    static uLong ZCALLBACK zlib_write( voidpf opaque,
                                       voidpf stream,
                                       const void* buf,
                                       uLong size )
    {
        Q_UNUSED( opaque );
        QIODevice *device = reinterpret_cast< QIODevice* >( stream );
        return device->write( static_cast< const char* >( buf ), size );
    }

    static ZPOS64_T ZCALLBACK zlib_tell( voidpf opaque,
                                         voidpf stream )
    {
        Q_UNUSED( opaque );
        QIODevice *device = reinterpret_cast< QIODevice* >( stream );
        return device->pos();
    }

    static long ZCALLBACK zlib_seek( voidpf opaque,
                                     voidpf stream,
                                     ZPOS64_T offset,
                                     int origin )
    {
        Q_UNUSED( opaque );
        QIODevice *device = reinterpret_cast< QIODevice* >( stream );
        switch ( origin ) {
        case ZLIB_FILEFUNC_SEEK_CUR:
            device->seek( device->pos() + offset );
            break;
        case ZLIB_FILEFUNC_SEEK_END:
            device->seek( device->size() - offset - 1 );
            break;
        case ZLIB_FILEFUNC_SEEK_SET:
            device->seek( offset );
            break;
        default:
            return -1;
        }
        return 0;
    }

    static int ZCALLBACK zlib_close( voidpf opaque,
                                     voidpf stream )
    {
        Q_UNUSED( opaque );
        Q_UNUSED( stream );
        return 0;
    }

    static int ZCALLBACK zlib_testerror( voidpf opaque,
                                         voidpf stream )
    {
        Q_UNUSED( opaque );
        QIODevice *device = reinterpret_cast< QIODevice* >( stream );
        return device->errorString().isEmpty();
    }

    static Archive::FileInfo fileInfoForCurrentFile( unzFile file )
    {
        unz_file_info64 fileInfo;
        QByteArray fileName;
        QByteArray comment;
            if ( unzGetCurrentFileInfo64( file,
                                          &fileInfo,
                                          0, 0,
                                          0, 0,
                                          0, 0 )
                 == UNZ_OK )
            {
                fileName.resize( fileInfo.size_filename + 1 );
                comment.resize( fileInfo.size_file_comment + 1 );
                if ( unzGetCurrentFileInfo64( file,
                                              0,
                                              fileName.data(), fileName.size(),
                                              0, 0,
                                              comment.data(), comment.size() )
                     == UNZ_OK )
                {
                    Archive::FileInfo fi( fileName,
                                          comment,
                                          fileInfo.uncompressed_size,
                                          fileInfo.compressed_size );
                    fi.values().insert( "zip.method", static_cast< int >( fileInfo.compression_method ) );
                    return fi;
                } else
                {
                    return Archive::FileInfo::createError( QT_TR_NOOP_UTF8( "Failed to get information from file in archive" ) );
                }
            } else
            {
                return Archive::FileInfo::createError( QT_TR_NOOP_UTF8( "Failed to get information from file in archive" ) );
            }
    }

private slots:

    void currentIODeleted( QObject *io )
    {
        if ( currentFileIO == io )
        {
            currentFileIO = 0;
        }
    }

};

/**
  @endcond
 */

/**
  @class ZipArchive
  @brief Implementation of the AIO::Archive class for the Zip file format

  The ZipArchive class provides support for reading and writing Zip files.
  Please refer to the documentation of the overridden methods in order to
  learn the specialties when dealing with zip files via the ArchIO framework.
 */
ZipArchive::ZipArchive(QIODevice *device, QObject *parent) :
    Archive(device, parent),
    d_ptr( new ZipArchivePrivate( this ) )
{
}

ZipArchive::~ZipArchive()
{
}

Archive::FileInfoList ZipArchive::listFiles()
{
    Q_D( ZipArchive );
    FileInfoList result;
    unzFile file = unzOpen2_64( d->device(), &d->fileFuncs );
    if ( file )
    {
        if ( unzGoToFirstFile( file ) == UNZ_OK )
        {
            unz_file_info64 fileInfo;
            QByteArray fileName;
            QByteArray comment;
            do
            {
                if ( unzGetCurrentFileInfo64( file,
                                              &fileInfo,
                                              0, 0,
                                              0, 0,
                                              0, 0 )
                     == UNZ_OK )
                {
                    fileName.resize( fileInfo.size_filename + 1 );
                    comment.resize( fileInfo.size_file_comment + 1 );
                    if ( unzGetCurrentFileInfo64( file,
                                                  0,
                                                  fileName.data(), fileName.size(),
                                                  0, 0,
                                                  comment.data(), comment.size() )
                         == UNZ_OK )
                    {
                        FileInfo fi( fileName,
                                     comment,
                                     fileInfo.uncompressed_size,
                                     fileInfo.compressed_size );
                        result << fi;
                    } else
                    {
                        setLastErrorString( tr( "Failed to get information from file in archive" ) );
                    }
                } else
                {
                    setLastErrorString( "Failed to get information from file in archive" );
                }
            } while ( unzGoToNextFile( file ) == UNZ_OK );
        } else
        {
            setLastErrorString( tr( "Failed to go to first file in archive" ) );
        }
        unzClose( file );
    } else
    {
        setLastErrorString( tr( "Unable to open Zip archive" ) );
    }
    return result;
}

QIODevice *ZipArchive::readFile(const FileInfo &file)
{
    if ( !file.isValid() ) {
        setLastErrorString( tr( "Invalid FileInfo passed to readFile()" ));
        return 0;
    }
    Q_D( ZipArchive );
    if ( d->currentFileIO ) {
        delete d->currentFileIO;
        d->currentFileIO = 0;
    }
    unzFile zfile = unzOpen2_64( d->device(), &d->fileFuncs );
    if ( zfile ) {
        if ( unzLocateFile( zfile, file.fileName().constData(), 0 ) ==
             UNZ_OK )
        {
            if ( unzOpenCurrentFile( zfile ) == UNZ_OK )
            {
                ZipArchiveFileReader *reader =
                        new ZipArchiveFileReader( zfile, d->fileInfoForCurrentFile( zfile ), this );
                d->currentFileIO = reader;
                return reader;
            } else
            {
                setLastErrorString(
                            tr( "Failed to open file %1 within Zip file for reading" ) );
            }
        } else
        {
            setLastErrorString( tr( "Failed to locate file %1 in archive" )
                                .arg( QString( file.fileName() ) ) );
        }
    } else {
        setLastErrorString( tr( "Failed to open zip file for reading." ) );
    }
    return 0;
}

/**
   @brief Creates a new file inside a zip archive.

   This will create a new file in the zip archive. Please refer to the
   documentation of the AIO::Archive::writeFile() method for a detailed
   description. For creating the file, the following properties of the
   @p file object are used:

   <table>
   <tr>
    <th>Property</th><th>Description</th>
   </tr>
   <tr>
    <td>fileName</td>
    <td>The name (and path) of the file to be created within the archive.</td>
   </tr>
   <tr>
    <td>comment</td>
    <td>An additional comment to attach to the file.</td>
   </tr>
   <tr>
    <td>level</td>
    <td>The compression level to use for the file. Defaults to 4 if not set.</td>
   </tr>
   <tr>
    <td>password</td>
    <td>The password to use to protect the file inside the archive.</td>
   </tr>
   <tr>
    <td>zip.forceZip32</td>
    <td>Force use of Zip32 format. If this is true, Zip32 format will be used.
        Defaults to false (meaning Zip64 format is used).</td>
   </tr>
   <tr>
    <td>zip.method</td>
    <td>The method to use for the file. See @AIO::ZipArchive::Method. Defaults to AIO::ZipArchive::DeflateMethod.</td>
   </tr>
   <tr>
    <td>zip.strategy</td>
    <td>The strategy to use. See AIO::ZipArchive::Strategy. Defaults to AIO::ZipArchive::DefaultStrategy.</td>
   </tr>
   </table>

*/
QIODevice *ZipArchive::writeFile(const FileInfo &file)
{
    if ( !file.isValid() )
    {
        setLastErrorString( tr( "Invalid FileInfo passed to writeFile()" ) );
        return 0;
    }

    Q_D( ZipArchive );
    if ( d->currentFileIO ) {
        delete d->currentFileIO;
        d->currentFileIO = 0;
    }
    zipFile zfile = zipOpen2_64( d->device(),    // file path
                                d->device()->size() == 0 ? 0 : 1,              // append
                                0,              // global comment,
                                &d->fileFuncs );
    if ( zfile ) {
        zip_fileinfo fi;
        // TODO: Fill info structure properly
        memset( &fi, 0, sizeof( fi ) );
        int errNo;
        if (  ( errNo = zipOpenNewFileInZip4_64( zfile,
                                      file.fileName().constData(),
                                      &fi,
                                      0, // extrafield local
                                      0, // size of ext.field local
                                      0, // extra field global
                                      0, // size of ext.field global
                                      file.comment().isNull() ? 0 : file.comment().constData(), // comment
                                      file.values().value( "zip.method", static_cast< int >( DeflateMethod ) ).toInt(), // method
                                      file.level( 4 ), // level
                                      0, // raw?
                                      -MAX_WBITS,  // window bits
                                      DEF_MEM_LEVEL, // memory level
                                      file.values().value( "zip.strategy", static_cast< int >( DefaultStrategy ) ).toInt(), // strategy
                                      file.password().isNull() ? 0 : file.password().constData(), // password
                                      0, // CRC (crypting)
                                      36, // version made by, assume v3.6 of the standard
                                      0, // flag base
                                      file.values().value( "zip.forceZip32" ).toBool() ? 0 : 1 // zip64?
                                      ) ) == ZIP_OK ) {
            d->currentFileIO = new ZipArchiveFileWriter( zfile, file, this );
            return d->currentFileIO;
        } else {
            setLastErrorString( QString( "Unable to create file in archive. Error %1" ).arg( errNo ) );
        }
    } else {
        setLastErrorString( tr( "Failed to open zip file for writing." ) );
    }
    return 0;
}

void ZipArchive::changeDevice(QIODevice *oldDevice, QIODevice *newDevice)
{
    if ( oldDevice ) {
        // todo: unset any references to old device
    }

    if ( newDevice ) {
        // todo: set any references to old device
    }
}

}
