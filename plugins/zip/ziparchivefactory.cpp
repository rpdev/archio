/*
    ArchIO - a library for accessing archive formats in Qt applications
    Copyright (C) 2013, 2014  Martin Hoeher

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

/**
  @cond INTERNAL
  */

#include "ziparchivefactory.h"

#include "ziparchive.h"

#include <QStringList>

namespace AIO {

ZipArchiveFactory::ZipArchiveFactory() :
    ArchiveFactory( tr( "Zip" ), QStringList() << "*.zip" )
{
}

Archive *ZipArchiveFactory::createArchive(QIODevice *device, QObject *parent)
{
    return new ZipArchive( device, parent );
}

bool ZipArchiveFactory::supportsDevice(QIODevice *device)
{
    Q_UNUSED( device );
    // TODO: Implement me!
    return false;
}

} // namespace AIO

/**
  @endcond
  */
