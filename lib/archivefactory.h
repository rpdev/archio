/*
    ArchIO - a library for accessing archive formats in Qt applications
    Copyright (C) 2013, 2014  Martin Hoeher

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#ifndef AIO_ARCHIVEFACTORY_H
#define AIO_ARCHIVEFACTORY_H

#include "archive.h"
#include "archio_global.h"

#include <QObject>

namespace AIO {

class ArchiveFactoryPrivate;
class ARCHIOSHARED_EXPORT ArchiveFactory : public QObject
{
    Q_OBJECT
    Q_PROPERTY( QString formatName READ formatName CONSTANT )
public:
    explicit ArchiveFactory(const QString& formatName,
                            const QStringList& formatFilter,
                            QObject *parent = 0);
    virtual ~ArchiveFactory();

    QString formatName() const;
    QStringList formatFilter() const;

    virtual Archive *createArchive(QIODevice *device , QObject *parent = 0 );
    virtual bool supportsDevice( QIODevice *device );


signals:

public slots:

private:

    QScopedPointer< ArchiveFactoryPrivate > d_ptr;
    Q_DECLARE_PRIVATE( ArchiveFactory )

};

} // namespace AIO

#endif // AIO_ARCHIVEFACTORY_H
